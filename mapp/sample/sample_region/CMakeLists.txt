set (MAINSRC_LIST sample_region.c)
add_executable (sample_region ${MAINSRC_LIST})
target_link_libraries (sample_region log sample_confparser mpp_vi mpp_isp vencoder mpp_vo mpp_component media_mpp ISP MemAdapter media_utils)

add_custom_target (copysample_region
    ALL
    COMMAND cp sample_region.conf ${PROJECT_BINARY_DIR}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
add_dependencies (sample_region copysample_region)

add_definitions (-DCOMMIT=)
