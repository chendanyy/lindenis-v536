一、busybox-1.27.2编译
    制作方法参考：https://blog.csdn.net/weixin_40639467/article/details/105036127?utm_medium=distribute.pc_relevant.none-task-blog-title-1&spm=1001.2101.3001.4242
    问题一：OpenWrt中静态编译APP时出现__aeabi_unwind_cpp_pr0的解决办法
      在链接的时候加上 '-lc -lgcc_eh'即可解决
      打开Makefile加上 LDLIBS      := -lc -lgcc_eh

二、系统定制
   1、修改文件系统大小
   target/allwinner/v536-Lindenis_SBC/defconfig
   CONFIG_TARGET_ROOTFS_PARTSIZE=100

   2、添加nfs支持
   执行make menuconfig
   选择Kernel modules--->Filesystems--->kmod-fs-nfs
   执行make kernel_menuconfig
   选择File systems--->Network File Systems--->NFS client support
                                           --->NFS client support for NFS version 3
                                           --->NFS client support for the NFSv3 ACL protocol extension

   3、添加telnet支持
   执行make menuconfig
   选择Base system--->busybox敲回车
   选择Login/Password Management Utilities--->login
                                          ---> Run logged in session in a child process
                                          ---> Support login scripts
                                          ---> Support /etc/nologin
                                          ---> Support /etc/securetty
                                          --->passwd
                                          ---> Check new passwords for weakness
   选择Networking Utilities--->telnet
                           ---> Pass TERM type to remote host
                           ---> Pass USER type to remote host
                           ---> Enable window size autodetection
                           --->telnetd
                           ---> Support standalone telnetd (not inetd only)
                           --->  Support -w SEC option (inetd wait mode)

   4、自启动及修改telnet root密码
   root@tina:/# passwd root
   打开/etc/init.d/rcS 添加telnetd -p 2223 &

   5、终端支持ctrl-r
   执行make menuconfig
   选择Base system--->busybox敲回车
   选择Busybox Library Tuning--->History saving
                             --->Reverse history search

   6、支持gdb调试
   执行make menuconfig
   选择Development->gdb

三、调试开发
   1、使用NFS文件系统进行开发
   #ubuntu:安装nfs服务
   sudo apt-get install nfs-kernel-server
   mkdir /home/qt/spring
   sudo vim /etc/exports
   添加/home/qt/spring *(rw,sync,no_root_squash,no_subtree_check,insecure,nohide)
   重启nfs服务sudo service nfs-kernel-server restart
   确定共享目录是否存在showmount -e
   #开发板：挂载NFS文件系统
   mount -t nfs -o nolock 192.168.9.61:/home/qt/spring /tmp

   2、编译
   #系统编译：
   参考http://wiki.lindeni.org/index.php/Lindenis_V536
   #交叉编译：
   打开/etc/profile 添加
   export PATH="/home/qt/work/lindenis-v536/prebuilt/gcc/linux-x86/arm/toolchain-sunxi-musl/toolchain/bin:$PATH"
   export STAGING_DIR="/home/qt/work/lindenis-v536/prebuilt/gcc/linux-x86/arm/toolchain-sunxi-musl/toolchain/bin:$STAGING_DIR"

   3、mpp开发

四、sample
   mapp目录下包含了mpp开发所需的库和头文件及sample；
   可在此目录开发自己的app
